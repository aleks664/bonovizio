export const anchors = (selector) => {
	const arrAnchors = document.querySelectorAll(selector)
	arrAnchors.forEach($button => {
		$button.addEventListener('click', (e) => {
			e.preventDefault()
			const id = $button.getAttribute('href') || $button.getAttribute('data-href')
			const $el = document.getElementById(id.substring(1))
			if ($el) {
				let display = window.getComputedStyle($el).display
				if (display === 'none') {
					$el.style.display = 'block'
				}
				const elPosition = $el.getBoundingClientRect().top
				if (display === 'none') {
					$el.style.display = 'none'
				}
				window.scrollBy({
					top: elPosition,
					behavior: 'smooth'
				})
			}
		})
	})
}
export const validate = (selector) => {
	const inputsValidate = document.querySelectorAll(selector)
	inputsValidate.forEach($input => {
		$input.addEventListener('invalid', (e) => {
			e.preventDefault()
			$input.classList.add('is-invalid');
			if ($input.closest('.custom-select')) {
				$input.closest('.custom-select').classList.add('is-invalid');
			}
		})
		$input.addEventListener('focus', () => {
			$input.classList.remove('is-invalid')
			if ($input.closest('.custom-select')) {
				$input.closest('.custom-select').classList.add('is-invalid');
			}
			if ($input.getAttribute('type') === 'radio') {
				const radios = $input.closest('form').querySelectorAll(`input[name="${$input.getAttribute('name')}"]`)
				radios.forEach($radio => {
					$radio.classList.remove('is-invalid')
				})
			}
		})
	})
}
