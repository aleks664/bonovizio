const burger = document.getElementById('burger');
const mobileMenu = document.getElementById('mobile-dropdown')
burger.addEventListener('click', (e) => {
	e.preventDefault();
	document.documentElement.classList.toggle('is-menu-open');
	mobileMenu.classList.toggle('is-open');
	burger.classList.toggle('is-active');
})
