import Swiper, {
	Navigation, Pagination
} from 'swiper';

const arrCarousel = document.querySelectorAll('.s-speakers');
arrCarousel.forEach(carousel => {
	let prev = carousel.querySelector('.s-speakers__prev');
	let next = carousel.querySelector('.s-speakers__next');
	let pagination = carousel.querySelector('.s-speakers__swiper-pagination');
	let $carousel = carousel.querySelector('.swiper');
	let slidesSm = carousel.dataset.slidesSm ? carousel.dataset.slidesSm : 1;
	let slidesMd = carousel.dataset.slidesMd ? carousel.dataset.slidesMd : 2;
	let slidesLg = carousel.dataset.slidesLg ? carousel.dataset.slidesLg : 2;
	// eslint-disable-next-line no-unused-vars
	const swiper = new Swiper($carousel, {
		modules: [Navigation, Pagination],
		loop: true,
		slidesPerView: 1,
		spaceBetween: 0,
		navigation: {
			nextEl: prev,
			prevEl: next
		},
		pagination: {
			el: pagination,
			clickable: true
		},
		breakpoints: {
			320: {
				slidesPerView: slidesSm
			},
			768: {
				slidesPerView: slidesMd,
				spaceBetween: 41
			},
			992: {
				slidesPerView: slidesLg,
				spaceBetween: 38
			}
		}
	});
})
